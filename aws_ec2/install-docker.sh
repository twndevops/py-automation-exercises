#!/bin/bash

sudo yum update -y
sudo yum install -y docker
sudo service docker start
sudo chown ec2-user /var/run/docker.sock
docker run -d --name nginx -p 80:80 nginx