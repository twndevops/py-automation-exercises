# AWS Automation Scripting with Python

## Table of Contents

1. [About](#about)
2. [Requirements](#requirements)
3. [Requirements for #3](#requirements-for-3)
4. [Requirements for #4](#requirements-for-4)
5. [Requirements for #5](#requirements-for-5)
6. [Steps for #5](#steps-for-5)

## About

1. `aws_subnets/*`: Boto3 Client and Resource implementations of a script that prints the IDs of all the subnets in your configured/ default AWS region.

2. `aws_iam/*`: Client and Resource implementations of a script that prints the ID and name of the most recently active IAM user created in your root AWS account.

3. `aws_ec2/*`: Client implementation of a script that programmatically

- creates and configures rules for an EC2 firewall (i.e. Security Group)
- provisions a new EC2 and attaches the previously created Sec Group
- waits until the EC2 is fully initialized
- connects to the EC2
- copies over and runs the `install-docker.sh` script on the EC2. This script installs Docker, starts the Docker daemon, and spins up a containerized Nginx web server accessible on host port 80.
- defines and calls a scheduled job that pings the Nginx app running in the container
- restarts the Nginx container if it's unresponsive for 1 minute

4. `aws_ecr/*`: 2 Client implementations of a script that prints all image tags (sorted newest to oldest) in every repository in the 1) Private registry and 2) Public registry.

5. `aws_pipeline/*`: This Jenkins pipeline

- fetches all the images from one of your private ECR repos
- asks you to select 1 image to deploy
- connects to a previously created EC2 instance
- authenticates from the EC2 to ECR to pull the image
- runs your selected image as a container on the EC2
- pings the app to validate that it's up and accessible

All of these tasks are modularized into 3 Python scripts run in different stages of the pipeline.

## Requirements

- To run the scripts for #1-4 above, you can directly clone this project. To run #5 (the pipeline), you must fork this repo.

- Run `aws configure` locally. Provide the Access and Secret Keys of the IAM user account and the region in which Boto3 will manage resources.

- Ensure you have Python 3 and Boto3 (the Python SDK for AWS) installed locally (`pip install boto3`).

### Requirements for #3:

- In `aws_ec2/ec2-docker-nginx.py`, on lines 71 / 114 / 138, replace `tf-jenkins-key-pair` filename / filepath with the name of / path to your SSH key. If you don't have any, manually create a new key pair, and download and store the private key safely in your local .ssh directory.

- Your default region's default VPC should have DNS hostnames and resolution enabled so that created EC2s will have public hostnames.

- After running this script, load the app URL `http://[server_IP/hostname]:80` in the browser to see Nginx running.

### Requirements for #4:

- If you don't have any images in either ECR registry, first create a repo in ECR. Build 3 [multi-architecture](https://www.docker.com/blog/multi-arch-build-and-images-the-simple-way/) Docker image versions based on one of your applications (modify the app code slightly from 1.0 to 2.0).

- See your ECR repo's `View push commands` modal for the commands to tag and push your images to the repo. If you created a private/ public repo, run the private/ public script, respectively.
- Example ECR structure:

  - Private / Public registry
    - `nodejs-app` repo
      - 1.0 tag
      - 2.0 tag

- If you're going to run the project's Jenkins pipeline, create a private repo.

### Requirements for #5:

To run this pipeline, you should know how to:

- manually create SSH key pairs and ECR repositories in AWS.
- create Docker images from your applications.
- run Ansible playbooks.
- install plugins and create pipeline builds and credentials in Jenkins.

### Steps for #5:

1. Fork this repo.

#### AWS setup

2. Run the `aws_ec2/ec2-docker-nginx.py` script to launch an EC2 and install/ start Docker on the instance. See `Requirements for 3`.

3. See `Requirements for 4`.

#### Jenkins setup

4. To quickly spin up an EC2 instance running Jenkins in a Docker container, run [this Ansible playbook](https://gitlab.com/twndevops/ansible-exercises/-/blob/24ac350368ef8c3d7c01a21f9e572c31225b8a18/playbooks/5-run-jenkins-container-ec2-amznlinux.yaml). For instructions on how to run this playbook, see the project [README](https://gitlab.com/twndevops/ansible-exercises#playbook-5).

5. Manually SSH into the EC2, enter the Jenkins container as the `root` user: `docker exec -it -u 0 [container_id] /bin/bash`, and install the necessary Python packages for the scripts to run:

- `apt install python3-pip`
- `apt install python3-boto3`
- `apt install python3-paramiko`
- `apt install python3-requests`
- Docker and Python3 should already be installed.

6. In the Jenkins UI:

- create a new pipeline for this project. Configure your fork to be the remote repository.
- install the [`Pipeline: Input Step`](https://plugins.jenkins.io/pipeline-input-step/) plugin.
- configure (global or project-scoped) Secret text credentials for your IAM user account's Access and Secret Keys.
- configure a (global or project-scoped) SSH private key credential for the private key of the EC2 that will run your ECR image. The username should be `ec2-user`.
- configure a (global or project-scoped) Username and password credential for your ECR registry login. The username should be `AWS`. Run `aws ecr get-login-password` to output the ECR password. (The ECR password may change over time. If this occurs, re-run the command and edit the Jenkins credential to use the latest password.)

#### Jenkinsfile changes

7. Replace the values of:

- `AWS_DEFAULT_REGION` with your AWS account's configured/ default region.
- `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY` with the names of your Jenkins Secret text credentials.
- `ECR_URL` with your private registry's URL.
- `ECR_REPO` with your private repo's name.

8. In the stage `Run selected image as a container on EC2`, replace:

- the outer `withCredentials` credential with the name of your Jenkins SSH private key credential for the EC2 that will run your ECR image.
- the inner `withCredentials` credential with the name of your Jenkins Username and password credential to login to ECR.

#### Python script changes

9. In `aws_pipeline/run-on-ec2.py`, on line 29, replace 4000 with the port where your app's server will run.

#### Run the pipeline!

10. Commit the above changes and push to your remote fork.

11. From the Jenkins UI, run the build.

- When the stage `Fetch ECR repo images and ask user to select version to deploy` runs, the build will wait for you to select the ECR image to deploy on the EC2 from a dropdown. Select your desired tag and the build will continue.

12. Once the build is complete, load the app URL `http://[server_IP/hostname]:80` in the browser to see your app running.
