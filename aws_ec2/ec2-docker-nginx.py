from boto3 import client
import paramiko
from scp import SCPClient
import time
import schedule
from requests import get

ec2_client = client("ec2")

# Create new Nginx sec group

nginx_sec_group_id = ec2_client.create_security_group(
    Description="Sec Group for Nginx Server",
    GroupName="Nginx"
    # No VpcId provided -> creates in default VPC
)["GroupId"]

ec2_client.authorize_security_group_ingress(
    GroupId=nginx_sec_group_id,
    IpPermissions=[
        {
            'FromPort': 22,
            'ToPort': 22,
            'IpProtocol': 'tcp',
            'IpRanges': [
                {
                    'CidrIp': '0.0.0.0/0',
                    'Description': 'SSH'
                },
            ],
        },
        {
            'FromPort': 443,
            'ToPort': 443,
            'IpProtocol': 'tcp',
            'IpRanges': [
                {
                    'CidrIp': '0.0.0.0/0',
                    'Description': 'HTTPS'
                },
            ],
        },
        {
            'FromPort': 80,
            'ToPort': 80,
            'IpProtocol': 'tcp',
            'IpRanges': [
                {
                    'CidrIp': '0.0.0.0/0',
                    'Description': 'HTTP'
                },
            ],
        }
    ]
)

ec2_client.authorize_security_group_egress(
    GroupId=nginx_sec_group_id,
    IpPermissions=[
        {
            # allows all protocol traffic on all ports
            'IpProtocol': '-1'
        }
    ]
)

# Start EC2 instance in default VPC, attach above sec group
ec2_instance = ec2_client.run_instances(
    ImageId="ami-0a3c3a20c09d6f377",
    InstanceType="t2.micro",
    KeyName="tf-jenkins-key-pair",
    MaxCount=1,
    MinCount=1,
    Monitoring={
        "Enabled": False
    },
    SecurityGroupIds=[
        nginx_sec_group_id
    ]
)["Instances"][0]

# Newly created EC2 is in "pending" state, wait until fully initialized

while True:
    ec2_instance_status_info = ec2_client.describe_instance_status(
        InstanceIds=[
            ec2_instance["InstanceId"]
        ],
        # include not-yet-running instances
        IncludeAllInstances=True
    )["InstanceStatuses"][0]

    if ec2_instance_status_info["InstanceStatus"]["Status"] == "ok" and ec2_instance_status_info["SystemStatus"]["Status"] == "ok":
        print("Initialization complete - 2/2 checks passed!")
        break

# Get EC2's public hostname

running_instance_dns = ec2_client.describe_instances(
    Filters=[
        {
            "Name": "instance-state-name",
            "Values": [
                "running"
            ]
        }
    ]
)["Reservations"][0]["Instances"][0]["PublicDnsName"]

# SSH localhost (127.0.0.1) into EC2

ssh_client = paramiko.client.SSHClient()
ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
ssh_client.connect(running_instance_dns, username="ec2-user", key_filename="/Users/upasananatarajan/.ssh/tf-jenkins-key-pair.pem")

# copy install script to EC2

scp_client = SCPClient(ssh_client.get_transport())
scp_client.put('/Users/upasananatarajan/Documents/TWNDevOps/14/py-automation-exercises/aws_ec2/install-docker.sh', remote_path="/home/ec2-user")

[stdin, stdout, stderr] = ssh_client.exec_command("bash install-docker.sh")
# MUST SLEEP FOR SCRIPT TO WORK!
time.sleep(5)

nginx_container_id = stdout.readlines()[len(stdout.readlines())-1].split("\n")[0]
print(nginx_container_id)

ssh_client.close()

# Create a scheduled function that sends a request to the nginx app and checks its status.
# If status is not OK 5 times in a row, the function should restart the container.


def restart_container():
    print("Restarting nginx container...")
    ssh_client = paramiko.client.SSHClient()
    ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh_client.connect(running_instance_dns, username="ec2-user", key_filename="/Users/upasananatarajan/.ssh/tf-jenkins-key-pair.pem")

    ssh_client.exec_command(f"docker stop {nginx_container_id}")
    ssh_client.exec_command(f"docker start {nginx_container_id}")

    ssh_client.close()


failed_response_count = 0


def ping_nginx():
    try:
        global failed_response_count
        # EC2 port 80 maps to container port 80
        # incl HTTP protocol in get URL!
        # disable redirection from HTTP to HTTPS
        response = get(f"http://{running_instance_dns}:80", allow_redirects=False)
        print(response.text)
        print(response.status_code)

        if response.status_code == 200:
            # reset count to 0 if app is back up, only increment if continuously down
            failed_response_count = 0
        else:
            failed_response_count += 1

        if failed_response_count == 5:
            print("5 failed connections, restarting container!")
            restart_container()
    except:
        failed_response_count += 1

        if failed_response_count == 5:
            print("5 failed connections, restarting container!")
            restart_container()


schedule.every(20).seconds.do(ping_nginx)

while True:
    schedule.run_pending()
