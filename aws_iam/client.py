from boto3 import client
# from datetime module, import datetime class
from datetime import datetime

# Get all IAM users in your AWS account
iam_client = client("iam")

all_users = iam_client.list_users()["Users"]

# For each user, print out the name of the user and when they were last active
today = datetime.today()
# initialize following to difference between today and a date before AWS acct creation
last_logged_in_timedelta = today - datetime.strptime("2023-01-20", "%Y-%m-%d")
last_logged_in_user = {}

for user in all_users:
    # print(user)
    print(user["UserName"])
    # skip users that never logged in (i.e. password never used)
    if "PasswordLastUsed" in user.keys():
        last_logged_in_time = user["PasswordLastUsed"].replace(tzinfo=None)
        print(last_logged_in_time)

# Print out the user ID and name of the user who was active the most recently

        # if curr user was latest to log in
        if today - last_logged_in_time < last_logged_in_timedelta:
            last_logged_in_timedelta = today-last_logged_in_time
            last_logged_in_user = user

# if no user ever logged in, last_logged_in_user would be empty dict
if "UserId" and "UserName" in last_logged_in_user.keys():
    print(last_logged_in_user["UserId"])
    print(last_logged_in_user["UserName"])
