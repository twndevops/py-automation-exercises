from boto3 import resource

# Get all subnets in your default region
# create ServiceResource
ec2_resource = resource("ec2")

# assume there may be more than 1 VPC
vpc_ids = []

# access ServiceResource's vpcs collection, call all() to get an iterable of all VPC resources
all_vpcs = ec2_resource.vpcs.all()
for vpc in all_vpcs:
    # append id of curr VPC resource to ids list
    vpc_ids.append(vpc.id)

print(vpc_ids)

all_subnets = []

for id in vpc_ids:
    # create new VPC resource
    vpc_resource = ec2_resource.Vpc(id)
    # access VPC resource's subnets collection, call all() to get an iterable of all subnets resources
    # extend() appends the iterable of subnet resources to subnets list
    all_subnets.extend(vpc_resource.subnets.all())

print(all_subnets)

# Print the subnet Ids
for subnet in all_subnets:
    # print id of curr Subnet resource
    print(subnet.id)
