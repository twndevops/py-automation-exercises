from boto3 import client
import os

ecr_client = client("ecr", region_name=os.environ['AWS_DEFAULT_REGION'], aws_access_key_id=os.environ['AWS_ACCESS_KEY_ID'], aws_secret_access_key=os.environ['AWS_SECRET_ACCESS_KEY'])

# ECR checks private registry repos only
images = ecr_client.list_images(repositoryName=os.environ["ECR_REPO"])["imageIds"]

for image in images:
    print(image["imageTag"])
