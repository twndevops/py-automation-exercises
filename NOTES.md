#### Notes for "Module 14 - Automation with Python" exercises

EXERCISE 1: AWS Subnets

- 2 implementations:
  - `EC2 Client`
    - Has 1 less loop b/c you can `describe_subnets` for all `VpcId`s at once.
    - `describe_subnets` method outputs a list of subnet dicts (low-lvl).
  - `EC2 Resource`
    - `Vpc` resource's `subnets` collection's `all` method outputs a list of `Subnet` resources: `ec2.Subnet(id='subnet-0a03904460dca6920')`. Resource = high-lvl Client wrapper.

EXERCISE 2: AWS IAM

- No important differences b/w `Client` and `Resource` implementations.
- NOTES on `datetime` class:
  - ERROR: `TypeError: can't subtract offset-naive and offset-aware datetimes`.
    - FIX: `dt.replace(tzinfo=None)` to remove the timezone info from user dict's `PasswordLastUsed`/ User resource's `password_last_used`.
  - CANNOT compare timedelta and datetime, or timedelta and float. CAN compare 2 timedelta objects.

EXERCISE 3: Automate running and monitoring a `nginx` container on an EC2 instance
- Created a new sec group `Nginx` w/ ingress rules allowing all IPs to access EC2 ports 22 (SSH), 80 (HTTP), and 443 (HTTPS). The `nginx` container will be accessed only via HTTP requests (i.e. via EC2 port 80).

- EC2 instance specifications:
  - AMZN Linux 2023 image, x86 (`linux/amd64/v3` arch). Pulled `nginx` image will be built for `amd64` platform.
  - Attached `Nginx` sec group.
  - NOTE: Default VPC has DNS hostnames and resolution both enabled, so EC2s created in this VPC will have public hostnames.

- EC2 instance obj returned by `run_instances()` does NOT have a `PublicDnsName` (it's an empty string). After EC2 is initialized, used `describe_instances()` to get `PublicDnsName`.

- `install-docker.sh` script:
  - installs Docker on the EC2
    - GENERAL NOTE: `cat /etc/os-release` to get EC2 OS. Always check Linux distro to find right Docker install instructions. 
    - Amazon Linux 2 / 2023 [Docker install instructions](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/install-docker.html) NOTE: AWS doesn't allow SSH'ing as root user to install Docker. Use `sudo` and `-y` to auto-confirm installs.
  - starts a `nginx` container
  - maps EC2 (host) port 80 to nginx container port 80.
    - EC2 ingress rule allows all HTTP traffic to port 80.
    - Loading an EC2's public DNS without specifying a port -> default HTTP port (80) on EC2 -> port 80 on `nginx` container.
      - To load a secure URL, must set up SSL/TLS for Nginx.

- 5 secs' sleep time after executing `bash install-docker.sh` is required for script to work! References:
  - https://github.com/paramiko/paramiko/issues/1617#issuecomment-611771433
  - https://stackoverflow.com/questions/60037299/attributeerror-nonetype-object-has-no-attribute-time-paramiko


EXERCISE 4: AWS ECR

- `ECR` is for Private registry repos. `ECRPublic` is for Public registry repos.

  - `ECR` and `ECRPublic` APIs don't have `ServiceResource`, only `Client`.
  - Calling `describe_registries()["registries"]` on either Client returns 1 registry dict since there's only 1 private and 1 public registry in ECR.

- Created 2 public and 2 private repos to do exercise w/ `ECR` and `ECRPublic` APIs.

  - Takeaway: Implementations are NOT DIFFERENT AT ALL, except they use a different Client.

- Used `nodejs-app` and `java-maven-app` to create Docker images. Created 2 diff version tags for each app.

  - NOTE: If creating images from same code, image IDs will be the same (confirm w/ `docker images`). In ECR, tags w/ same image IDs are listed as 1 entry (ex. 2.0, 1.0) with the timestamp of first tag pushed.
  - To create diff entries in ECR, made following changes in the apps:
    - `nodejs-app`: Dockerfile copies `app` dir. In `app/server.js`, used diff server ports (3000 and 4000) for diff tags.
    - `java-maven-app`: Dockerfile copies `target` dir's JAR file. In `pom.xml`, used diff versions (1.1.0-SNAPSHOT and 2.0-SNAPSHOT). Did not need to change app code to create diff image IDs.

- Final ECR structure:

  - Private registry
    - java-maven-app repo (newest to oldest)
      - 1.1.0-SNAPSHOT
      - 2.0-SNAPSHOT
    - nodejs-app repo (newest to oldest)
      - 1.0, 2.0 (same image ID)
      - 3.0
  - Public registry
    - java-maven-app repo (newest to oldest)
      - 1.1.0-SNAPSHOT
      - 2.0-SNAPSHOT
    - nodejs-app repo (newest to oldest)
      - 1.0, 2.0 (same image ID)
      - 3.0

- `sort()` modifies lists in-place. `sorted()` returns new sorted list from any iterable.

EXERCISE 5: Write a Jenkins pipeline that fetches all images from an ECR repo, asks a user to select 1 to deploy, and pulls/ runs the image on an EC2 instance.

MANUAL SETUP STEPS:

- Launched an EC2 instance. Specifications:

  - AMZN Linux 2023 image
    - 64-bit Arm, NOT x86! All ECR images use platform `linux/arm64/v8`. Containers will NOT start on x86 (`linux/amd64/v3` arch) EC2s.
  - Instance type `t4g.small`
  - SSH key `tf-jenkins-key-pair`
  - Created/ attached new sec group allowing SSH/ Internet HTTPS/ Internet HTTP from all IPs (`0.0.0.0/0` for all).
  - Defaults for all other settings.
    - NOTE: Default VPC has DNS hostnames and resolution both enabled, so EC2s created in this VPC will have public hostnames.

- Installed Docker on EC2 using cmds in `aws_ec2/install-docker.sh`.

- Entered Jenkins container (running on DigitalOcean droplet) as `root` user: `docker exec -it -u 0 [container_id] /bin/bash`.

  - Docker and Python3 (3.11.2-1+b1) were already installed in Jenkins container.
  - Installed necessary Py pkgs:
    - `apt install python3-pip`
    - `apt install python3-boto3`
    - `apt install python3-paramiko`
    - `apt install python3-requests`

- ECR Private registry setup:
  - nodejs-app repo
    - 1.0, 2.0 (same image ID, different tags)
      - Express server listening on port 3000.
    - 3.0
      - Express server listening on port 4000.

PIPELINE OVERVIEW:

- Py scripts can access pipeline env vars using `os.environ()` since scripts are running in the Jenkins container environment.

  - Env vars created in a stage (i.e. `env.ECR_TAG`) can be used in scripts run in a different stage (i.e. `run-on-ec2.py`). They are not scoped to that stage only and can be used in Py scripts.
  - Extracted credentials (i.e. `EC2_KEYFILE`, `ECR_USR`, `ECR_PSW`) are also env vars available in Py scripts. (See docs: https://www.jenkins.io/doc/book/pipeline/jenkinsfile/)

- `fetch-ecr-images.py` prints fetched images' tags in new lines. The pipeline stores this output to present to the user to select 1 to deploy using Jenkins' pre-installed `Pipeline: Input Step` plugin.

- In Jenkins UI:

  - Created a multibranch build for this repo: `jenkins-py-ecr-ec2/main`.
  - Created a "SSH Username with private key" credential scoped to this build only, `py-ec2-key` to store the contents of private key `tf-jenkins-key-pair.pem` Jenkins used to SSH into the EC2 instance.

- In `run-on-ec2`:

  - It's critical to describe only running EC2s!

    - If stopped EC2s are returned, `paramiko`'s `connect` method will fail b/c a stopped EC2 has no public hostname.

  - `docker login` to ECR fails w/o any `stdout`/ `stderr` if the global Jenkins credential `aws-ecr-creds-global` has an incorrect password.

    - Update the credential if this behavior occurs when running the build. Run `aws ecr get-login-password` locally to get the most up-to-date ECR password.
    - Once login succeeds, the pipeline will output a success message and an auth token will be added to `/home/ec2-user/.docker/config.json`.

  - NOTES on EC2 networking configuration:
    - The EC2 sec group has ingress rules allowing all IPs to access ports 22 (SSH), 80 (HTTP), and 443 (HTTPS). The `nodejs-app` repo images can be accessed only via HTTP requests.
    - When executing `docker run`, map `host (EC2) port 80` allowing all HTTP traffic to `app container port` (`3000` if running `1.0` or `2.0` tags, `4000` if running `3.0` tag).
    - Use `http://[EC2_public_hostname]:80` to load the app in the browser. ISSUE: App currently returns a `404` response.

- In `request-to-container.py`:
  - It's critical to describe only running EC2s!
  - `get` request MUST have a protocol, and MUST use HTTP, prevent redirect to HTTPS, and append `host (EC2) port 80` to ping app container.
    - NETWORKING NOTE: `Jenkins` server is running all Py scripts and using Py libs to SSH into or request to the EC2 instance, NOT the app container. So to request the container, requests MUST go via the EC2 HTTP port.
