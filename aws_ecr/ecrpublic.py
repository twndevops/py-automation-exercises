from boto3 import client
from operator import itemgetter

ecr_public_client = client("ecr-public")

# Get all the repositories in ECR
all_public_repos = ecr_public_client.describe_repositories()["repositories"]

all_public_repo_names = []

# Print the name of each repository
for repo in all_public_repos:
    all_public_repo_names.append(repo["repositoryName"])
    print(repo["repositoryName"])

# Choose one specific repository and for that repository, list all the image tags inside, sorted by date, where the most recent image tag is on top

for name in all_public_repo_names:
    curr_repo_images = ecr_public_client.describe_images(repositoryName=name)["imageDetails"]

    print(f"Newest to oldest tags for {name} repo:")

    sorted_by_imagePushedAt = sorted(curr_repo_images, key=itemgetter("imagePushedAt"), reverse=True)

    for image in sorted_by_imagePushedAt:
        print(image["imageTags"])
