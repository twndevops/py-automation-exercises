from boto3 import client

# Get all subnets in your default region
ec2_client = client("ec2")

# assume there may be more than 1 VPC
vpc_ids = []

all_vpcs = ec2_client.describe_vpcs()["Vpcs"]
for vpc in all_vpcs:
    vpc_ids.append(vpc["VpcId"])

print(vpc_ids)

all_subnets = ec2_client.describe_subnets(
    Filters=[
        {
            "Name": "vpc-id",
            "Values": vpc_ids
        }
    ]
)["Subnets"]

print(all_subnets)

# Print the subnet Ids
for subnet in all_subnets:
    print(subnet["SubnetId"])
