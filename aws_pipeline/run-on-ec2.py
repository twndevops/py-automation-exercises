from boto3 import client
import os
import paramiko

# SSH into the EC2 server
ec2_client = client("ec2", region_name=os.environ['AWS_DEFAULT_REGION'], aws_access_key_id=os.environ['AWS_ACCESS_KEY_ID'], aws_secret_access_key=os.environ['AWS_SECRET_ACCESS_KEY'])

ec2_instance = ec2_client.describe_instances(
    Filters=[
        {
            "Name": "instance-state-name",
            "Values": [
                "running"
            ]
        }
    ]
)["Reservations"][0]["Instances"][0]

ssh_client = paramiko.client.SSHClient()
ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
ssh_client.connect(ec2_instance["PublicDnsName"], username="ec2-user", key_filename=os.environ["EC2_KEYFILE"])

# Run docker login to auth with ECR repo
[stdin, stdout, stderr] = ssh_client.exec_command(f'echo {os.environ["ECR_PSW"]} | docker login --username {os.environ["ECR_USR"]} --password-stdin {os.environ["ECR_URL"]}')
print(stdout.readlines()[0].split("\n")[0])

# Start a container from the selected image on EC2
# Use container port 3000 for 1.0 and 2.0 tags, port 4000 for 3.0 tag
[stdin, stdout, stderr] = ssh_client.exec_command(f'docker run -d --name {os.environ["ECR_REPO"]} -p 80:4000 {os.environ["ECR_URL"]}/{os.environ["ECR_REPO"]}:{os.environ["ECR_TAG"]}')
print("Container ID", stdout.readlines()[0].split("\n")[0])

ssh_client.close()
