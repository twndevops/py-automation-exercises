from boto3 import client
import os
from requests import get

ec2_client = client("ec2", region_name=os.environ['AWS_DEFAULT_REGION'], aws_access_key_id=os.environ['AWS_ACCESS_KEY_ID'], aws_secret_access_key=os.environ['AWS_SECRET_ACCESS_KEY'])

ec2_instance = ec2_client.describe_instances(
    Filters=[
        {
            "Name": "instance-state-name",
            "Values": [
                "running"
            ]
        }
    ]
)["Reservations"][0]["Instances"][0]

# EC2 port 80 maps to container port 4000
# incl HTTP protocol in get URL!
# disable redirection from HTTP to HTTPS
response = get(f'http://{ec2_instance["PublicDnsName"]}:80', allow_redirects=False)
print(response.text)
print(response.status_code)
